﻿using System.Threading.Tasks;

namespace eThor.BsPoc
{
    public class MyBogusClass
    {
        public Task SomeLocalMethod()
        {
            var localVariable = "Why hullo thar";
            //TODO: Some more things, and then some more things
            return Task.CompletedTask;
        }

        public Task SomeOtherLocalMethod()
        {
            var localVariable = "BACON";
            return Task.CompletedTask;
        }

        public Task PremMethod()
        {
            var localVariable = "MAPLEBACON!";
            return Task.CompletedTask;
        }

        public Task WinBackupsMethod()
        {
            var localVariable = "Added for testing windows local agent pushes";
            return Task.CompletedTask;
        }

        public Task SecondWinBackupsTest()
        {
            var localVariable = "something for testing";
            return Task.CompletedTask;
        }

        public Task TestingBuildToolsBambooVar()
        {
            var localVariable = "using the \${bamboo.my.build.tools.path}";
            return Task.CompletedTask;
        }
    }
}
